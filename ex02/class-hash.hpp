// Hash table
// By: KatoMono In: 17-Jan-12017 HE

#ifndef HASH
#define HASH 0

typedef int Type;

class OHT {
  private:
    Type* table;
    bool* used;
    int cap;
    int size;
    inline int hashValue(Type v);

  public:
    OHT(int n = 13);
    ~OHT();

    void insert(Type v);
    void remove(Type v);
    void print();
    bool search(Type v);

    int getCap();
};

struct Menu {
  OHT* table;

  Menu(OHT* t);
  void print();
  inline void insert();
  void remove();
  void random();
  void start();
};

#endif
