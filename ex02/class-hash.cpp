// Hash table
// By: KatoMono In: 17-Jan-12017 HE

#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <string>
#include "class-hash.hpp"
using namespace std;

OHT::OHT(int n) {
  cap = n;
  size = 0;
  table = new Type[n];
  used = new bool[n];
}

OHT::~OHT() {
  size = cap = 0;
  delete[] table;
  delete[] used;
}

inline int OHT::hashValue(Type i) {
  return (i % cap);
}

void OHT::insert(Type v) {
  // Tabela cheia
  if(size == cap) {
    cerr << "Tabela cheia, item não incluído" << endl;
  } else {
    // Achar a chave
    int k = hashValue(v);
    // Se o espaço estiver vazio
    if(used[k]) {
      // Achar a proxima posição vazia
      while(used[k]) {
        ++k;
        k %= cap;
      }
    }
    // Marcar como usado
    used[k] = true;
    // Incrementar tamanho
    ++size;
    // Escrever dado
    table[k] = v;
  }
}

void OHT::remove(Type v) {
  // Tabela vazia
  if(!size) {
    cerr << "Tabela vazia" << endl;
  }

  // Achar a key
  int k = hashValue(v);

  if(table[k] == v) {
    used[k] = false;
  } else {
    int aux = k;
    do {
      ++k;
      k %= cap;
    } while((k != aux) and (table[k] != v));

    if(k != aux) {
      used[k] = false;
    } else {
      cerr << "Elemento não encontrado" << endl;
    }
  }
}

void OHT::print() {
  int w = (cap > 0 ? (int) log10 ((double) cap) + 1 : 1);

  for(int i = 0; i < cap; ++i) {
    cout << setw(w) << i << " : ";

    if(used[i]) {
      cout << table[i];
    }

    cout << "\n";
  }
}

int OHT::getCap() {
  return cap;
}

Menu::Menu(OHT* t) {
  table = t;
}

inline void Menu::print() {
  table->print();
}

void Menu::insert() {
  cout << "Insira qunatos inteiros positivos a ser inseridos: ";
  int a;
  cin >> a;
  cout << endl;
  cerr << "a: " << a << endl;

  cout << "Digite os numeros a serem inseridos." << endl;
  for(int i = 0; i < a; ++i) {
    Type n;
    cin >> n;
    table->insert(n);
  }
  cout << endl;
}

void Menu::remove() {
  cout << "Digite o item a ser removido: ";
  Type n;
  cin >> n;
  table->remove(n);
}

void Menu::random() {
  cout << "Digite o números de item aletorios a ser inserido: ";
  int n;
  cin >> n;
  cout << endl;

  srand(time(0));
  for(int i = 0; i < n; ++i) {
    table->insert(rand());
  }
}

void Menu::start() {
  char op;
  do {
    cout << endl
         << "Opções" << endl
         << "  (I)nserir" << endl
         << "  (A)leatório" << endl
         << "  (L)istar" << endl
         << "  (R)emover" << endl
         << "  (S)air" << endl
         << endl
         << "> ";

    cin >> op;
    op = toupper(op);
    cout << endl;

    switch(op) {
      case 'I': insert(); break;
      case 'L': print(); break;
      case 'R': remove(); break;
      case 'A': random(); break;
      case 'S': cout << "See ya, then :3" << endl; break;
      default: cout << "Opção inválida" << endl; break;
    }
  } while(op != 'S');
}
