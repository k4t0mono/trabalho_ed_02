// Hash table
// By: KatoMono In: 17-Jan-12017 HE

#include <iostream>
#include "class-hash.hpp"
using namespace std;

int main() {
  cout << "*** Tabela Hash com Endereçamento Aberto ***" << endl << endl;

  cout << "Digite o tamanho da tabela: ";
  int n;
  cin >> n;
  cout << endl;

  OHT table(n);
  Menu(&table).start();

  return 0;
}
