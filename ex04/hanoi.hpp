// hanoi.hpp

#ifndef HANOI
#define HANOI 0

enum Tower {A, B, C};

struct Move {
  Tower orign;
  Tower dest;
};

class HT {
  private:
    int size;
    Move* tree;

  public:
    HT(int n);
    ~HT();
};

#endif
