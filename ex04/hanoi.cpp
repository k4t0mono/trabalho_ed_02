#include <iostream>
using namespace std;

enum Tower {A, B, C};
 
struct Move {
  Tower origem;
  Tower dest;
  
  friend ostream& operator<<(ostream& out, Move& mv);
};

ostream& operator<<(ostream& out, Move& mv) {
  out << mv.origem << " -> " << mv.dest;
  
  return out;
}

class Hanoi {
  private:
    int size;
    Tower* tree;
    
    Tower otherTower(int i);
    void percorreEmOrdemAux();
    void arrumaAux(int i);
  
  public:
    Hanoi(int n);
    ~Hanoi();
    
    void percorreEmOrdem();
};

Hanoi::Hanoi(int n) {
  size = n;
  tree = new Tower[n];
}

Tower Hanoi::otherTower(int i) {
  Tower mv = tree[i];
  
  if(mv.origem == A) {
    if(mv.dest == B) {
      return C;
    } else {
      return B;
    }
  } else fi(mv.origem == B) {
    if(mv.dest == A) {
      return C;
    } else {
      return A;
    }
  } else {
    if(mv.dest == B) {
      return C;
    } else {
      return B;
    }
  }
}

void Hanoi::percorreEmOrdem() {
  percorreEmOrdemAux(0);
  cout << endl;
}

void Hanoi::percorreEmOrdemAux(int atual) {
  if (atual < n) {
    percorreEmOrdemAux(2*n+1);
    cout << atual << endl;
    percorreEmOrdemAux(2*n+2);
  }
}

// Colocar no construtor
void arruma() {
  tree[0] = {A, C};
  arruma(1);
  arruma(2);
}

void Hanoi::arrumaAux(int i) {
  if(i < n) {
  if(esq(pai(i)) == i) {
    tree[i].origem = tree[pai(i)].origem;
    tree[i].dest = otherTower(i);
  } else {
    tree[i].origem = otherTower(i);
    tree[i].dest = tree[pai(i)].dest;
  }
  arrumaAux(2*i+1);
  arrumaAux(2*i+2);
  }
}
