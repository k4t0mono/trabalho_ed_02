#include <iostream>
using namespace std;

enum Tower {A, B, C};
 
struct Move {
  Tower origem;
  Tower dest;
  
  friend ostream& operator<<(ostream& out, Move& mv);
};

ostream& operator<<(ostream& out, Move& mv) {
  out << mv.origem << " -> " << mv.dest;
  
  return out;
}

class Hanoi {
  private:
    int size;
    Move* tree;
    
    Tower otherTower(int i);
    void percorreEmOrdemAux(int i);
    void arrumaAux(int i);
  
  public:
    Hanoi(int n);
    ~Hanoi();
    
    void percorreEmOrdem();
};

Hanoi::Hanoi(int n) {
  size = n;
  tree = new Move[n];
  
  tree[0] = {A, C};
  arruma(1);
  arruma(2);
}

Tower Hanoi::otherTower(int i) {
  Move mv = tree[i];
  
  if(mv.origem == A) {
    if(mv.dest == B) {
      return C;
    } else {
      return B;
    }
  } else if(mv.origem == B) {
    if(mv.dest == A) {
      return C;
    } else {
      return A;
    }
  } else {
    if(mv.dest == B) {
      return C;
    } else {
      return B;
    }
  }
}

void Hanoi::percorreEmOrdem() {
  percorreEmOrdemAux(0);
  cout << endl;
}

void Hanoi::percorreEmOrdemAux(int atual) {
  if (atual < n) {
    percorreEmOrdemAux(2*n+1);
    cout << atual << endl;
    percorreEmOrdemAux(2*n+2);
  }
}

void Hanoi::arrumaAux(int i) {
  if(i < n) {
    if(esq(pai(i)) == i) {
      tree[i].origem = tree[pai(i)].origem;
      tree[i].dest = otherTower(i);
    } else {
      tree[i].origem = otherTower(i);
      tree[i].dest = tree[pai(i)].dest;
    }
    arrumaAux(2*i+1);
    arrumaAux(2*i+2);
  }
}
