// graph.hpp

#ifndef GRAPH_CLASS
#define GRAPH_CLASS 0

#include <list>

class Graph {
  private:
    // Numero de nós
    int n_nodes;
    // Uma lista de links para cada nó
    std::list<int>* links;

  public:
    Graph(int n);
    ~Graph();

    // Adiciona um link de v para w
    void addLink(int v, int w);
    // Cria os links apartir de uma matriz
    void create(bool** matriz);
    // Verifica se há um acminho entre x e y
    bool hasPath(int x, int y);
};

#endif
