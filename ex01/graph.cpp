// graph.cpp

#include <iostream>
#include "graph.hpp"

Graph::Graph(int n) {
  n_nodes = n;
  links = new std::list<int>[n];
}

Graph::~Graph() {
  n_nodes = 0;
  delete[] links;
}

void Graph::create(bool** matriz) {
  for(int i = 0; i < n_nodes; ++i) {
    for(int j = 0; j < n_nodes; ++j) {
      // Se o elemeto for 1
      if(matriz[i][j]) {
        // Adiciona um link de v para w, inculindo w na lista de posição v
        links[i].push_back(j);
      }
    }
  }
}

// Verifica se há um acminho entre x e y
bool Graph::hasPath(int x, int y) {
  // Caso base
  if(x == y) {
    return true;
  }

  // Marcar todos nós como não visitados
  bool visited[n_nodes];
  for(int i = 0; i < n_nodes; ++i) {
    visited[i] = false;
  }

  // Criar uma fila para auxiliar na pesquisa
  std::list<int> q;

  // Marcar x como visitado e enfilerar
  visited[x] = true;
  q.push_back(x);

  // Enquanto a lista não for fazia
  while(!q.empty()) {
    // Desenfilerar o nó
    x = q.front();
    q.pop_front();

    // Percore todos os links que saem de x
    for(std::list<int>::iterator i = links[x].begin(); i != links[x].end(); ++i) {
      // Se o valor for o procurado
      if(*i == y) {
        // Retorne verdadeiro
        return true;
      }

      // Se o nó não foi visitado
      if(!visited[*i]) {
        // Marcar como visitado
        visited[*i] = true;
        // Enfileirar o nó
        q.push_back(*i);
      }
    }
  }

  return false;
}
