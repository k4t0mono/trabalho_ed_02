// main.cpp

#include <iostream>
#include "graph.hpp"
using namespace std;

int main() {
  cout << "Digite o numero de nodes: ";
  int n;
  cin >> n;
  cout << endl;

  bool** matriz = new bool*[n];
  for(int i = 0; i < n; ++i) {
    matriz[i] = new bool[n];
  }
  cout << "Digite a matriz de adjacencia:" << endl;
  for(int i = 0; i < n; ++i) {
    for(int j = 0; j < n; ++j) {
      cin >> matriz[i][j];
    }
  }
  cout << endl;

  Graph g(n);
  g.create(matriz);

  cout << "Digite as posicoes dos nodes A e B" << endl
    << "Para verificar se existe uma rota de A para B: ";
  int a, b;
  cin >> a >> b;
  cout << endl << endl;

  if(g.hasPath(a, b)) {
    cout << "Exite uma rota de A para B.";
  } else {
    cout << "Nao exite uma rota de A para B.";
  }
  cout << endl;

  for(int i = 0; i < n; ++i) {
    delete[] matriz[i];
  }
  delete[] matriz;

  return 0;
}
